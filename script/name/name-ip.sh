#!/bin/bash

rm script/name/name.txt
docker inspect -f '{{.Name}} - {{.NetworkSettings.IPAddress }} - {{.Args}}' $(docker ps -aq) | sed 's/^.//' >script/name/name.txt
