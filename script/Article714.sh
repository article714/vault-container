#! /bin/bash

#docker run --cap-add=IPC_LOCK -d  --name loca article714/vault-container:test vault-raft
#docker build -f Dockerfile -t article714/vault-container:test .

#List of vaults ( name and type)
#names=$(cat script/name/name.txt | awk 'NF==5' | awk '{print $1}')
#ips=$(cat script/name/name.txt | awk 'NF==5' | awk '{print $3}')
./script/name/name-ip.sh

##############
#Password.txt#
##############

function password {
    local names=$(cat script/name/name.txt | awk '{print $1}')
    local l="container/config/vault/password.txt"
    local r="/container/config/vault/"
    ./script/pass.sh
    echo -e "\n"
    read -s -p 'Choose a vault name : ' vault
    echo $vault
    echo -e "\n"
    if [ "$vault" ]; then
        echo "====== Send the password.txt to $vault container ======"
        docker cp "$l" "$vault:$r"
    else
        echo "You have selected a wrong vault name or a wrong vault."
    fi
}

function initialize {

    echo "====== Backing up vault: ======"
    echo -e "\n"
    read -s -p 'Choose a vault name : ' vault
    echo $vault
    echo -e "\n"
    local raft=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-raft" | awk '{print $1}')
    local transit=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-transit" | awk '{print $1}')
    docker cp container/config/vault/password.txt $vault:/container/config/vault/
    if [ "$raft" ]; then
        echo "====== Initialize the vault $vault ======"
        docker exec -i $vault bash <container/config/vault/vault-raft/initialize.sh
        mkdir container/vault/$vault/
        docker cp $vault:/container/config/vault/key-encrypt.txt container/vault/$vault/
        sleep 2
        docker cp $vault:/container/config/vault/token2.txt container/vault/$vault/
        docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh
    else
        if [ "$transit" ]; then
            echo "====== Initialize the vault $vault ======"
            read -s -p 'What is the name of the vault that will allow unlocking? : ' seal
            echo $seal
            echo -e "\n"
            echo "====== Initialize and unlock the vault transit ======"
            echo -e "\n"
            script/modif.sh $vault $seal

            if [ "$vault" ]; then
                echo "====== Send the token to $vault container ======"
                docker cp container/vault/$seal/token2.txt $vault:/container/config/vault/
            else
                echo "You have selected a wrong vault name"
            fi
            echo "====== Start the $vault vault ======"
            docker exec -i $vault bash <container/config/vault/vault-transit/initialize.sh
            mkdir container/vault/$vault/
            docker cp $vault:/container/config/vault/recovery-key-encrypt.txt container/vault/$vault/
            docker cp $vault:/container/config/vault/token2.txt container/vault/$vault/
            docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh
        else
            echo "You have selected a wrong vault"
        fi
    fi
}

########
#Backup#
########

#save all vault raft
function snapshootAll {

    local names=$(cat script/name/name.txt | grep -i "vault" | awk '{print $1}')
    echo -e "\n"
    echo "====== Backing up vaults: ======"
    echo -e "\n"
    for name in ${names}; do
        echo "====== ${name[$i]} container ======"
        echo -e "\n"
        echo "====== Snapshoot ======"
        echo -e "\n"
        docker cp container/config/vault/password.txt $vault:/container/config/vault/
        docker exec -i ${name[$i]} bash <container/config/vault/vault-raft/snapshoot.sh
        sleep 2
        echo -e "\n"
        echo "====== Upload the backup ======"
        echo -e "\n"
        docker cp ${name[$i]}:/container/config/vault/BACKUP-Vault-raft.snap container/config/vault/
        echo -e "\n"
        echo "====== Backing up vaults: ======"
        echo -e "\n"
        mv container/config/vault/BACKUP-Vault-raft.snap container/backups/BACKUP-Vault-${name[$i]}.snap
        docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh
    done
}

#save one vault raft
function snapshootOne {
    echo "====== Backing up vault: ======"
    echo -e "\n"
    read -s -p 'Choose a vault name : ' vault
    echo $vault
    echo -e "\n"
    local raft=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-raft" | awk '{print $1}')
    local transit=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-transit" | awk '{print $1}')
    docker cp container/config/vault/password.txt $vault:/container/config/vault/
    if [ "$transit" ]; then
        echo "====== $vault container ======"
        echo -e "\n"
        docker exec -i $vault bash <container/config/vault/vault-transit/snapshoot.sh
        sleep 2
        echo -e "\n"
        echo "====== Upload the backup ======"
        echo -e "\n"
        docker cp $vault:/container/config/vault/BACKUP-Vault.snap container/config/vault/
        echo "====== Backing up vaults: ======"
        echo -e "\n"
        mv container/config/vault/BACKUP-Vault.snap container/backups/BACKUP-Vault-$vault.snap
        docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh
    else
        if [ "$raft" ]; then
            echo "====== $vault container ======"
            echo -e "\n"
            docker exec -i $vault bash <container/config/vault/vault-raft/snapshoot.sh
            sleep 2
            echo -e "\n"
            echo "====== Upload the backup ======"
            echo -e "\n"
            docker cp $vault:/container/config/vault/BACKUP-Vault.snap container/config/vault/
            echo "====== Backing up vaults: ======"
            echo -e "\n"
            mv container/config/vault/BACKUP-Vault.snap container/backups/BACKUP-Vault-$vault.snap
            docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh
        else
            echo "You have selected a wrong vault name or a wrong vault for the backup"
        fi
    fi
}
#save one vault raft
function restore {
    echo "====== Restore a vault: ======"
    echo -e "\n"
    read -s -p 'Choose a vault name : ' vault
    echo $vault
    echo -e "\n"
    read -s -p 'Which vault should be loaded? ' vault2
    echo $vault2
    echo -e "\n"
    echo "====== Restore the vault ======"
    echo -e "\n"
    local raft=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-raft" | awk '{print $1}')
    local transit=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-transit" | awk '{print $1}')
    if [ "$transit" ]; then
        docker cp container/config/vault/password.txt $vault:/container/config/vault/
        sleep 2
        docker cp container/backups/BACKUP-Vault-$vault2.snap $vault:/container/config/vault/
        sleep 2
        docker cp container/vault/$vault2/token2.txt $vault:/container/config/vault/
        cp container/vault/$vault2/token2.txt container/vault/$vault/
        cp container/vault/$vault2/recovery-key-encrypt.txt container/vault/$vault/
        sleep 2
        docker exec -i $vault bash <container/config/vault/vault-transit/restore.sh
        sleep 2
        docker cp container/vault/$vault2/recovery-key-encrypt.txt $vault:/container/config/vault/
        sleep 2
        docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh

    else
        if [ "$raft" ]; then
            docker cp container/config/vault/password.txt $vault:/container/config/vault/
            sleep 2
            docker cp container/backups/BACKUP-Vault-$vault2.snap $vault:/container/config/vault/
            sleep 2
            cp container/vault/$vault2/token2.txt container/vault/$vault/
            sleep 2
            docker exec -i $vault bash <container/config/vault/vault-raft/restore.sh
            sleep 2
            cp container/vault/$vault2/recovery-key-encrypt.txt container/vault/$vault/
            sleep 2
            docker cp container/vault/$vault2/recovery-key-encrypt.txt $vault:/container/config/vault/
            sleep 2
            #docker exec -i $vault bash <container/config/vault/vault-raft/delete.sh
        else
            echo "You have selected a wrong vault name or a wrong vault for the backup"
        fi
    fi
}

function restore2 {
    echo "====== Restore a vault: ======"
    echo -e "\n"
    read -s -p 'Choose a vault name : ' vault
    echo $vault
    echo -e "\n"
    local raft=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-raft" | awk '{print $1}')
    local transit=$(cat script/name/name.txt | grep -i "$vault" | grep -i "vault-transit" | awk '{print $1}')
    docker cp container/config/vault/password.txt $vault:/container/config/vault/
    if [ "$transit" ]; then
        docker cp container/backups/BACKUP-Vault-$vault.snap $vault:/container/config/vault/
        docker exec -i $vault bash <container/config/vault/vault-transit/restore2.sh
        sleep 5
        docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh

    else
        if [ "$raft" ]; then
            docker cp container/backups/BACKUP-Vault-$vault.snap $vault:/container/config/vault/
            docker exec -i $vault bash <container/config/vault/vault-raft/restore2.sh
            sleep 5
            docker exec -i $vault bash <container/config/vault/vault-raft/delete.sh
        else
            echo "You have selected a wrong vault name or a wrong vault for the backup"
        fi
    fi
}

function initialize2 {

    read -s -p 'What is the name of the vault that will initialize? : ' vault
    echo $vault
    echo -e "\n"
    echo -e "\n"
    local raft=$(cat script/name/name.txt | grep -i "$vault " | grep -i "vault-raft" | awk '{print $1}')
    local transit=$(cat script/name/name.txt | grep -i "$vault " | grep -i "vault-transit" | awk '{print $1}')
    docker cp container/config/vault/password.txt $vault:/container/config/vault/
    if [ "$raft" ]; then
        echo "====== Initialize and unlock the raft vault $vault ======"
        docker cp container/vault/$vault/key-encrypt.txt $vault:/container/config/vault/
        docker exec -i $vault bash <container/config/vault/vault-raft/initialize2.sh
        sleep 2
        docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh
    else
        if [ "$transit" ]; then
            echo "====== Initialize and unlock the transit vault $vault ======"
            read -s -p 'What is the name of the vault that will allow unlocking? : ' seal
            echo $seal
            echo -e "\n"
            docker cp container/vault/$seal/token2.txt $vault:/container/config/vault/
            docker exec -i $vault bash <container/config/vault/vault-transit/initialize2.sh
            sleep 2
            docker exec -i $vault bash <container/config/vault/vault-transit/delete.sh
        fi
    fi
}

######
#help#
######
function help {
    cat <<-EOF

USAGE:
  Select an action with a number

ACTIONS:

Send password
    Allows you to create the file and send it. Do not forget the password used. It will open the file containing the vault unlock key(s).

Initialize
    Allows you to initialize a vault for the first time

Snapshoot all
    Saves all running raft Vaults

Snapshoot one
    Save one running raft Vault

Initialize2
    Allows you to initialize a vault after a restart

Restore
    Restore data from a vault to a new vault

Restore2
    Restores data from a vault to the original vault

EOF
}

######
#Menu#
######
cat script/logo.txt
echo -e "\n"
PS3="
Choose the action you want to do: "
echo -e "\n"
action=("Send password" "Initialize" "Snapshoot all" "Snapshoot one" "Initialize2" "Restore" "Restore2" "Help" "Exit")
select fav in "${action[@]}"; do
    case $fav in
    "Send password")
        echo -e "\nSend password to start containers"
        password
        ;;
    "Initialize")
        echo -e "\nInitialize a vault"
        initialize
        ;;
    "Snapshoot all")
        echo -e "\nSave all raft vaults"
        snapshootAll
        ;;
    "Snapshoot one")
        echo -e "\nSave one raft vaults"
        snapshootOne
        ;;
    "Initialize2")
        echo -e "\nInitialize a vault after a restart"
        initialize2
        ;;
    "Restore")
        echo -e "\nRestore one raft vaults"
        restore
        ;;
    "Restore2")
        echo -e "\nRestore one raft vaults"
        restore2
        ;;

    "Help")
        echo -e "\nHelp :"
        help
        ;;
    "Exit")
        echo -e "\nYou are leaving the Lease4 - Vault tool"
        exit
        ;;
    *) echo -e "\nInvalid option $REPLY" ;;
    esac
done
