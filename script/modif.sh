#! /bin/bash

modif=$(cat script/name/name.txt | grep "$2 " | awk '{print $3}')

sed -i "s/172.17.0.2/$modif/" container/config/vault/vault-transit/vault.config
docker cp container/config/vault/vault-transit/vault.config $1:/container/config/vault/vault-transit/
sleep 2
sed -i "s/$modif/172.17.0.2/" container/config/vault/vault-transit/vault.config
