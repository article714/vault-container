# vault-container development

## Gitlab runner configuration

The runner for vault-container image tests lust be configured to add `IPC_LOCK` capability to any running container (see [doc](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnersdocker-section))
That particular runner must be associated to `docker_IPC_LOCK` Gitlab tag

Also for building images, we need to access some other services that are connected to an `internal_bridge` named docker network.
