# Version 0.3.0 (Work in progress)

- updated build system for multi-versions
- key-shares an key-thresholds are now setup as parameters
- Removed default volume bindings
- Changed versioning scheme to include targeted vault version
- Added some automated tests and security scanners
- Added some documentation
- Added tooling to support multiple use-cases
