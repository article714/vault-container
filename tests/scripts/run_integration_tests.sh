#!/bin/bash

# set -x

# setup used paths
LAUNCH_DIR=$(pwd)

MY_SCRIPT=$(realpath -s $0)
MY_SCRIPTPATH=$(dirname $MY_SCRIPT)
DOCKER_COMPOSE_PATH=$(realpath -s $MY_SCRIPTPATH/../docker)

# local test image identification
if [ -z "$IMAGE_VERSION" ]; then
    export IMAGE_VERSION="localtest"
fi
if [ -z "$IMAGE_NAME" ]; then
   export IMAGE_NAME=vault-container
fi

# Docker Env
if [ -z "$2" ]; then
  export project_name="a714"
else
  export project_name="$2"
fi


start(){
    #--------------------------
    # Let's Go and start the test env

    if [ -f "$DOCKER_COMPOSE_PATH/docker-compose.yml" ]; then
        docker-compose -p ${project_name} -f ${DOCKER_COMPOSE_PATH}/docker-compose.yml up  -d
        sleep 5
    else
        echo "FAIL: cannot find docker-compose file"
        exit -1
    fi
}

stop(){
    #---------------------------
    # Let's stop the testing env

    if [ -f "$DOCKER_COMPOSE_PATH/docker-compose.yml" ]; then
        docker-compose -p ${project_name} -f ${DOCKER_COMPOSE_PATH}/docker-compose.yml stop
    else
        echo "FAIL: cannot find docker-compose file"
        exit -1
    fi
}

remove(){
    #---------------------------
    # Let's stop the testing env

    if [ -f "$DOCKER_COMPOSE_PATH/docker-compose.yml" ]; then
        docker-compose -p ${project_name} -f ${DOCKER_COMPOSE_PATH}/docker-compose.yml $@
    else
        echo "FAIL: cannot find docker-compose file"
        exit -1
    fi
}

case $1 in
    cleanup)
        remove rm -v
    ;;
    restart)
        stop
        start
    ;;
    start)
        start
    ;;
    stop)
        stop
    ;;
    wipeout)
        stop
        remove down -v
    ;;
    *)
        start
    ;;
esac


