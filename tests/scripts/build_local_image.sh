#!/bin/bash

# set -x

is_wsl=$(uname -a | grep microsoft | grep -c WSL)

# versions management
PRODUCT_VERSION=1.10.2-1
DEBIAN_CONTAINER_VERSION=bullseye-0.9.0

# local test image identification
IMAGE_VERSION="localtest"
IMAGE_NAME=vault-container
CI_COMMIT_BRANCH="__local__"

# Docker build opts
#BUILD_OPTS='--force-rm --no-cache'
BUILD_OPTS=''
DOCKER_ADDTL_PARAMS="--build-arg PRODUCT_VERSION=${PRODUCT_VERSION} --build-arg  DEBIAN_CONTAINER_VERSION=${DEBIAN_CONTAINER_VERSION} --build-arg A714_COMMIT_TAG=${CI_COMMIT_BRANCH}"


#---------------
# Let's build image

echo " Building ${IMAGE_NAME}:${IMAGE_VERSION}"
docker build ${BUILD_OPTS} -t article714/${IMAGE_NAME}:${IMAGE_VERSION} --build-arg IMAGE_VERSION=${IMAGE_VERSION} ${DOCKER_ADDTL_PARAMS} .
