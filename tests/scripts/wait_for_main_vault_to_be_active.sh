#!/bin/bash

# Wait for Transit vault to start
x=10
vault_started="down"
while [ $x -gt 0 -a "$vault_started" != "run" ]; do
   sleep 5
   x=$(($x-1))
   vault_started=$(docker exec ${project_name}_vault-transit_1 sv status vault | cut -d ':' -f 1 || true)
   echo "INFO: Waiting for transit vault to start: $vault_started"
done
if [ "$vault_started" = "down" ]; then
   echo "ERROR: Transit Vault did not start"
   exit -1
fi

x=10

# Wait for Transit vault to be unsealed
sealed="true"
echo "INFO: Testing when Transit Vault is still unsealed"
while [ $x -gt 0 -a "$sealed" != "false" ]; do
   sleep 5
   x=$(($x-1))
   sealed=$(docker exec ${project_name}_vault-transit_1 vault status | grep Sealed | sed -e "s/\\W\\W*/-/g" | cut -d '-' -f 2 || true )
   echo "INFO: Waiting for transit vault to be unsealed: $sealed"
done
if [ "$sealed" = "true" ]; then
   echo "ERROR: Transit Vault is still sealed"
   exit -1
fi


# Wait for secrets to be sent to Main Vault
docker exec  ${project_name}_vault-main_1 /bin/bash -c "x=10;while [ \$x -gt 0 -a  ! -f \"/container/config/vault/secrets/autounseal-testunseal-encrypted-token.txt\" ]; do echo \"waiting for main vault to receive secrets\";x=\$((\$x-1)); sleep 5; done;"


# Wait for Main vault to be started
x=10
vault_started="down"
while [ $x -gt 0 -a "$vault_started" != "run" ]; do
   sleep 5
   x=$(($x-1))
   vault_started=$(docker exec ${project_name}_vault-main_1 sv status vault | cut -d ':' -f 1 || true )
   echo "INFO: Waiting for main vault to start: $vault_started"
done
if [ "$vault_started" = "down" ]; then
   echo "ERROR: Vault did not start"
   exit -1
fi


# Wait for Main vault to be unsealed
x=10
sealed="true"
while [ $x -gt 0 -a "$sealed" != "false" ]; do
   sleep 5
   x=$(($x-1))
   sealed=$(docker exec ${project_name}_vault-main_1 vault status | grep Sealed | sed -e "s/\\W\\W*/-/g" | cut -d '-' -f 2 || true )
   echo "INFO: Waiting for main vault to be unsealed: $sealed"
done
if [ "$sealed" = "true" ]; then
   echo "ERROR: Vault is still sealed"
   exit -1
fi

# Wait for Main vault to be active
x=10
vault_mode="false"
while [ $x -gt 0 -a "$vault_mode" != "active" ]; do
   sleep 5
   x=$(($x-1))
   vault_mode=$(docker exec ${project_name}_vault-main_1 vault status | grep "HA Mode" | sed -e "s/\\W\\W*/-/g" | cut -d '-' -f 3 || true )
   echo "INFO: Waiting for main vault to be active: $vault_mode"
done
if [ "$sealed" = "true" ]; then
   echo "ERROR: Vault is still in ${vault_mode} Mode"
   exit -1
fi