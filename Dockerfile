ARG DEBIAN_CONTAINER_VERSION
#### Need to change this when merging ###
FROM article714/debian-based-container:${DEBIAN_CONTAINER_VERSION}
LABEL maintainer="C. Guychard @ Article714"


ARG PRODUCT_VERSION
ARG A714_COMMIT_TAG

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

# Container configuration:
ENV VAULT_X509_SUBJECT "/C=FR/ST=Brittany/L=Brest/O=Article714/OU=Secrets/CN=localhost"

ENV VAULT_FLAVOR="vault-default"
ENV VAULT_SECRETS_DIR="/container/config/vault/secrets"

ENV BACKUP_DIR="/container/backups"

ENV VAULT_TRANSIT_KEY_NAME="unsealdefaultkey"
ENV VAULT_TRANSIT_ADDRESS=""

# Vault parameters for recovery-shares & recovery-threshold
ENV VAULT_KTH=1
ENV VAULT_KSH=1

# Container tooling
COPY container /container

# container building

RUN /container/build.sh ${A714_COMMIT_TAG} ${PRODUCT_VERSION}


# 8200/tcp is the primary interface that applications use to interact with
# Vault.
EXPOSE 8200

# entrypoint  & default command
ENTRYPOINT [ "/container/entrypoint.sh" ]
CMD ["start"]

# healthcheck
HEALTHCHECK --interval=120s --timeout=2s --retries=5 CMD /container/check_running