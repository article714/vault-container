#!/bin/bash

# set -x

export LANG=en_US.utf8

A714_COMMIT_TAG=$1
PRODUCT_VERSION=$2

# Add Vault user
groupadd vault
adduser --no-create-home --disabled-password --shell /usr/sbin/nologin --gecos "" --ingroup vault vault

# install gnupg to enable adding of hashicorp keys
apt-get update
apt-get install -qy --no-install-recommends libcap2-bin gnupg gnupg-agent curl software-properties-common python3 python3-pip

# install pyhon3 additional deps
python3 -m pip install --upgrade pip
if [ $? -gt 0 ]; then
    echo "Could not upgrade Pip"
    exit -1
fi
python3 -m pip install -r /container/requirements.txt
if [ $? -gt 0 ]; then
    echo "Could not install python deps"
    exit -1
fi

# Install Vault
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
apt-get update
apt-cache policy vault
apt-get install -yq vault=${PRODUCT_VERSION}

# setup privileges for vault
setcap cap_ipc_lock=+ep /usr/bin/vault

# checks that some needed commands are installed
type vault
if [ $? -gt 0 ]; then
    echo "Cannot find Vault command"
    exit -1
fi
type python3
if [ $? -gt 0 ]; then
    echo "Cannot find python3 command"
    exit -1
fi

#--
# services activation
update-service --add $SERVICEDIR/vault

#-- directory initialization
mkdir -p ${BACKUP_DIR}
chown vault. ${BACKUP_DIR}
chown vault. /container/config/vault

#--
# Cleaning
apt-get -yq clean
apt-get -yq remove software-properties-common
apt-get -yq autoremove
rm -rf /var/lib/apt/lists/*

# cleanup useless cron jobs
rm -f /etc/cron.daily/passwd /etc/cron.daily/dpkg /etc/cron.daily/apt-compat
truncate --size 0 logs
truncate --size 0 /var/log/lastlog
truncate --size 0 /var/log/faillog
truncate --size 0 /var/log/dpkg.log
truncate --size 0 /var/log/syslog
