#!/bin/bash


# set -x


PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/bin

export SVDIR="/container/config/services"
export VAULT_CONFIG_FILE="/container/config/vault/vault.config"
export VAULT_ADDR="https://0.0.0.0:8200"
export VAULT_SECRETS_DIR="/container/config/vault/secrets"

initialize() {

    # Build self-signed Certificate (initial configuration)
    if [[ ! -f "/container/config/vault/certs/vault.crt" && -n "${VAULT_X509_SUBJECT}" ]]; then
        chown vault.root /container/config/vault/certs
        cd /container/config/vault/certs
        if [ -n "${VAULT_X509_ALTNAMES}" ]; then
            chpst -u vault openssl req -newkey rsa:4096 -days 1001 -nodes -x509 -subj ${VAULT_X509_SUBJECT} -addext "subjectAltName=${VAULT_X509_ALTNAMES}" -keyout "vault.key" -out "vault.crt"
        else
            chpst -u vault openssl req -newkey rsa:4096 -days 1001 -nodes -x509 -subj ${VAULT_X509_SUBJECT} -keyout "vault.key" -out "vault.crt"
        fi

    fi

    # Setup Vault (initial configuration)
    if [ ! -f ${VAULT_CONFIG_FILE} ]; then

        if [ -f /container/config/vault/templates/${VAULT_FLAVOR}.config ]; then
            cp /container/config/vault/templates/${VAULT_FLAVOR}.config ${VAULT_CONFIG_FILE}
        fi

        if [ "$VAULT_FLAVOR" != "vault-default" ]; then
            check-provided-secret
        fi

        sleep 1
        sync
        /container/config/vault/templates/${VAULT_FLAVOR}/initialize.sh

    fi
}

debug(){
    update-service --remove $SERVICEDIR/vault
    runsvdir -P ${SVDIR} &
    sleep 2
    initialize
    exit 0
}

initialize_and_kill(){
    update-service --remove $SERVICEDIR/vault
    runsvdir -P ${SVDIR} &
    sleep 2
    initialize
    sync
    sv stop vault
    kill -HUP $(pidof runsvdir)
    exit 0
}


start_with_autounseal_token(){
    if [ ! -f ${VAULT_CONFIG_FILE} ]; then
        update-service --remove $SERVICEDIR/vault
        sleep 1
        echo "Will run vault initialization in background"
        (sleep 3;initialize;/container/tools/generate_autounseal_token.sh;) &
    fi
    exec runsvdir -P ${SVDIR}
}


start(){
    if [ ! -d  /vault/storage ]; then
        mkdir -p /vault/storage
    fi
    chown vault. /vault/storage
    chown vault. /container/config/vault

    if [ ! -f ${VAULT_CONFIG_FILE} ]; then
        update-service --remove $SERVICEDIR/vault
        sleep 1
        echo "Will run vault initialization in background"
        (sleep 3; initialize) &
    fi
    exec runsvdir -P ${SVDIR}
}

check-provided-secret(){
    # We check that the password.txt exists
    if [ ! -f "/container/config/vault/secrets/password.txt" ]; then
        echo "ERROR: You need to provide a password.txt to encrypt the file containing the keys"
        exit -1
    fi
}


case "$1" in
shell)
    exec "/bin/bash"
    ;;
--)
    start
    ;;
start)
    start
    ;;
debug)
    set -x
    debug
    ;;
start_with_autounseal_token)
    start_with_autounseal_token
    ;;
initialize_and_kill)
    initialize_and_kill
    ;;
*)
    exec "$@"
    ;;
esac

exit 1
