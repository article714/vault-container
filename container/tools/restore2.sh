#!/bin/bash

#export VAULT_ADDR="https://127.0.0.1:8200"
export VAULT_SKIP_VERIFY=true

vault operator raft snapshot restore ${BACKUP_DIR}/BACKUP-Vault*
