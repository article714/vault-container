#!/bin/bash

export VAULT_SKIP_VERIFY=true

vault operator raft snapshot restore -force -tls-skip-verify /container/config/vault/BACKUP-Vault*

sleep 2

openssl enc -d -aes-256-cbc -pbkdf2 -in /container/config/vault/key-encrypt.txt -out /container/config/vault/key-nocrypt.txt -pass pass:$(cat /container/config/vault/password.txt)

sleep 2

vault operator unseal $(grep 'Key 1:' /container/config/vault/key-nocrypt.txt | awk '{print $NF}')

sleep 2

rm /container/config/vault/key-nocrypt.txt
rm /container/config/vault/password.txt
