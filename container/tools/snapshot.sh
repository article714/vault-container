#!/bin/bash

#
# Generate a Snapshot of current Vault data (if supported)
#

export VAULT_ADDR="https://127.0.0.1:8200"
export VAULT_SKIP_VERIFY=true

vault operator raft list-peers
sleep 2
vault operator raft snapshot save /container/config/vault/BACKUP-Vault.snap
