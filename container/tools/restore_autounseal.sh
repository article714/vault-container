#!/bin/bash

export VAULT_SKIP_VERIFY=true

openssl enc -d -aes-256-cbc -pbkdf2 -in /container/config/vault/recovery-key-encrypt.txt -out /container/config/vault/key1.txt -pass pass:$(cat /container/config/vault/password.txt)
sleep 2
vault login $(grep 'Initial Root Token:' /container/config/vault/key1.txt | awk '{print $NF}')
sleep 10
vault operator raft snapshot restore -force /container/config/vault/BACKUP-Vault*
sleep 5
