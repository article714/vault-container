#!/bin/bash

# set -x

#---
# Check parameters

if [ '$VAULT_TRANSIT_KEY_NAME' == "" ]; then
    echo "ERROR: you must provide name of the new unseal Key  VAULT_TRANSIT_KEY_NAME environment Variable"
    exit -1
fi


#---
# Create a new autounseal Key

jinja -D VAULT_TRANSIT_KEY_NAME "${VAULT_TRANSIT_KEY_NAME}" /container/config/vault/templates/vault-transit/autounseal.hcl.j2  > /container/config/vault/policies/autounseal-${VAULT_TRANSIT_KEY_NAME}.hcl

vault write -f transit/keys/${VAULT_TRANSIT_KEY_NAME}
if [ $? -ne 0 ]; then
    echo "ERROR: unable to create autounseal key"
    exit -1
fi

echo "Created autounseal key (${VAULT_TRANSIT_KEY_NAME})"
sleep 2

vault policy write autounseal /container/config/vault/policies/autounseal-${VAULT_TRANSIT_KEY_NAME}.hcl
echo "The policy was created"

#---
# Create a new Token for target vault
# TODO: manage the fact that root token might be re-generated

if [ -f ${VAULT_SECRETS_DIR}/key-encrypt.txt ] && [ -f ${VAULT_SECRETS_DIR}/password.txt ]; then
    # Decrypt secrets
    openssl enc -d -aes-256-cbc -pbkdf2 -out ${VAULT_SECRETS_DIR}/key.txt -in ${VAULT_SECRETS_DIR}/key-encrypt.txt -pass pass:$(cat ${VAULT_SECRETS_DIR}/password.txt)
    echo "Decrypted secret key"
else
    echo "ERROR: unable to decrypt secrets"
    exit -1
fi

if [ -f ${VAULT_SECRETS_DIR}/key.txt ]; then
    # Login before generating the secrets
    vault login $(grep 'Initial Root Token:' ${VAULT_SECRETS_DIR}/key.txt | awk '{print $NF}')  &> /dev/null
    if [ $? -ne 0 ]; then
        echo "ERROR: unable to login"
        exit -1
    fi
fi
sleep 2

#  Creating the client-token.txt
VAULT_WRAPPED_TOKEN=$(vault token create -policy="autounseal" -wrap-ttl=12000 | grep 'wrapping_token:'| awk '{print $NF}')

VAULT_TOKEN=${VAULT_WRAPPED_TOKEN} vault unwrap | grep 'token '  | awk '{print $NF}' > ${VAULT_SECRETS_DIR}/autounseal-${VAULT_TRANSIT_KEY_NAME}-client-token.txt

openssl enc -e -aes-256-cbc -pbkdf2 -out ${VAULT_SECRETS_DIR}/autounseal-${VAULT_TRANSIT_KEY_NAME}-encrypted-token.txt -in ${VAULT_SECRETS_DIR}/autounseal-${VAULT_TRANSIT_KEY_NAME}-client-token.txt -pass pass:$(cat ${VAULT_SECRETS_DIR}/password.txt)

rm -f ${VAULT_SECRETS_DIR}/autounseal-${VAULT_TRANSIT_KEY_NAME}-client-token.txt

echo "Created and Encrypted the client-token.txt secret"

# remove decrypted secret
if [ -f  ${VAULT_SECRETS_DIR}/key.txt ]; then
    rm -f ${VAULT_SECRETS_DIR}/key.txt
fi

# The END
echo "Created a new Auto-Unseal configuration"