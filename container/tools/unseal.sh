#!/bin/bash


export VAULT_SKIP_VERIFY=true
export VAULT_ADDR="https://0.0.0.0:8200"
export VAULT_SECRETS_DIR="/container/config/vault/secrets"


openssl enc -d -aes-256-cbc -pbkdf2 -in ${VAULT_SECRETS_DIR}/key-encrypt.txt -out ${VAULT_SECRETS_DIR}/key-nocrypt.txt -pass pass:$(cat ${VAULT_SECRETS_DIR}/password.txt)

for key_index in $(seq 1 $VAULT_KTH); do
    vault operator unseal $(grep "Key ${key_index}:" ${VAULT_SECRETS_DIR}/key-nocrypt.txt | awk '{print $NF}')
done
echo "Vault Unsealed"

rm -f ${VAULT_SECRETS_DIR}/key-nocrypt.txt