#!/bin/bash

# set -x


VAULT_CONFIG_FILE="/container/config/vault/vault.config"


# check parameters
if [ "$VAULT_TRANSIT_ADDRESS" == "" ]; then
    echo "ERROR: you need to provid Transit Vault using VAULT_TRANSIT_ADDRESS environment variable"
    exit -1
fi

if [ "$VAULT_TRANSIT_KEY_NAME" == "" ]; then
    echo "ERROR: you need to provid Transit Vault key name using VAULT_TRANSIT_KEY_NAME environment variable"
    exit -1
fi


# TODO => fix this, should be using a valid CA
export VAULT_SKIP_VERIFY=true

# Reveal Transit Vault Token

VAULT_CLIENT_TOKEN=$(openssl enc -d -aes-256-cbc -pbkdf2 -in ${VAULT_SECRETS_DIR}/autounseal-${VAULT_TRANSIT_KEY_NAME}-encrypted-token.txt -pass pass:$(cat ${VAULT_SECRETS_DIR}/password.txt))

# generate config file

jinja -D VAULT_TRANSIT_ADDRESS "${VAULT_TRANSIT_ADDRESS}"   -D VAULT_TRANSIT_KEY_NAME "${VAULT_TRANSIT_KEY_NAME}" -D VAULT_CLIENT_TOKEN "${VAULT_CLIENT_TOKEN}" /container/config/vault/templates/vault-autounseal.config.j2  > ${VAULT_CONFIG_FILE}

# add service and start
chown vault. /container/config/vault
update-service --add $SERVICEDIR/vault
sleep 5

vault operator init -recovery-shares=${VAULT_KSH} -recovery-threshold=${VAULT_KTH} >${VAULT_SECRETS_DIR}/recovery-key.txt
sleep 5
sync

# Encrypt secrets
openssl enc -e -aes-256-cbc -pbkdf2 -in ${VAULT_SECRETS_DIR}/recovery-key.txt -out ${VAULT_SECRETS_DIR}/recovery-key-encrypt.txt -pass pass:$(cat ${VAULT_SECRETS_DIR}/password.txt)
rm -f ${VAULT_SECRETS_DIR}/recovery-key.txt
rm -f ${VAULT_SECRETS_DIR}/autounseal-${VAULT_TRANSIT_KEY_NAME}-client-token.txt
echo "Encrypted secret key"


# Wait a bit for serve
sv status vault
echo "Initialized Vault with transit Unseal "

