#!/bin/bash

# set -x

update-service --add $SERVICEDIR/vault

# Wait a bit for server to be started
sleep 5
sv status vault

export VAULT_SKIP_VERIFY=true

vault operator init -key-shares=${VAULT_KSH} -key-threshold=${VAULT_KTH} >${VAULT_SECRETS_DIR}/key.txt
echo "Vault initialized"
sleep 5

for key_index in $(seq 1 $VAULT_KTH); do
    vault operator unseal $(grep "Key ${key_index}:" ${VAULT_SECRETS_DIR}/key.txt | awk '{print $NF}')  &> /dev/null
    if [ $? -ne 0 ]; then
        echo "ERROR: unable to unseal Vault"
        exit -1
    fi
done
echo "Vault Unsealed"
sleep 10

# Login before encrypting the secrets
vault login $(grep 'Initial Root Token:' ${VAULT_SECRETS_DIR}/key.txt | awk '{print $NF}')  &> /dev/null
if [ $? -ne 0 ]; then
    echo "ERROR: unable to login"
    exit -1
fi
sleep 2

# Encrypt secrets
openssl enc -e -aes-256-cbc -pbkdf2 -in ${VAULT_SECRETS_DIR}/key.txt -out ${VAULT_SECRETS_DIR}/key-encrypt.txt -pass pass:$(cat ${VAULT_SECRETS_DIR}/password.txt)
rm -f ${VAULT_SECRETS_DIR}/key.txt
echo "Encrypted secret key"

# Enable transit secret engine
vault secrets enable transit
if [ $? -ne 0 ]; then
    echo "ERROR: could not enable transit secret engine"
    exit -1
fi
sleep 5

