#!/bin/bash

# set -x

# add service and start

update-service --add $SERVICEDIR/vault

# Wait a bit for server to be started
sleep 5
sv status vault

# Initialize Vault in "default mode"

export VAULT_SKIP_VERIFY=true

vault operator init -key-shares=${VAULT_KSH} -key-threshold=${VAULT_KTH} >${VAULT_SECRETS_DIR}/key.txt
echo "Vault initialized"
sleep 2

for key_index in $(seq 1 $VAULT_KTH); do
    vault operator unseal $(grep "Key ${key_index}:" ${VAULT_SECRETS_DIR}/key.txt | awk '{print $NF}')    &> /dev/null
    if [ $? -ne 0 ]; then
        echo "ERROR: unable to unseal Vault"
        exit -1
    fi
done
echo "Vault Unsealed"

# Encrypt secrets
openssl enc -e -aes-256-cbc -pbkdf2 -in ${VAULT_SECRETS_DIR}/key.txt -out ${VAULT_SECRETS_DIR}/key-encrypt.txt -pass pass:$(cat ${VAULT_SECRETS_DIR}/password.txt)
rm -f ${VAULT_SECRETS_DIR}/key.txt
echo "Encrypted secret key"