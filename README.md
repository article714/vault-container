# vault-container

A container image hosting an [Hashicorp Vault](https://www.vaultproject.io/) server.

## Basic usage

### without TLS

Test configuration

```bash
docker run --cap-add=IPC_LOCK -d --name local_vault --env VAULT_FLAVOR="vault-autounseal" article714/vault-container
```

Vault "Flavors":

- `vault-default`
- `vault-autounseal`, a vault cluster node using raft storage and bound to a transit vault for auto-unseal
- `vault-transit`, a simple vault server that can be used to unseal other vaults

We use raft storage for `vault-autounseal` & `vault-transit`

Other Environment/configuration variables

- `BACKUP_DIR`: directory to store backups & snapshots, defaults to `/container/backups`
- `VAULT_X509_ALTNAMES`
- `VAULT_X509_SUBJECT`
- `VAULT_KTH`, defaults to 1, Vault parameters for recovery-shares & recovery-threshold | key-shares & key
- `VAULT_KSH`, defaults to 1, Vault parameters for recovery-shares & recovery-threshold
- `VAULT_TRANSIT_ADDRESS`, the Transit Vault to which an _autounseal_ one will connect
- `VAULT_TRANSIT_KEY_NAME`
- `VAULT_SECRETS_DIR`

TODO Document => ${VAULT_SECRETS_DIR}/key-encrypt.txt
how to encrypt / decrypt key

```shell
openssl enc -d -aes-256-cbc -pbkdf2 -in key-encrypt.txt  -pass pass:$(cat password.txt)

```

## A "transit" Vault

```shell
docker run -ti --cap-add=IPC_LOCK --rm --env VAULT_FLAVOR="vault-transit" -v $(pwd):/container/config/vault/secrets article714/vault-container:localtest
```

We need to provide a `VAULT_SECRETS_DIR`
and a storage for data : `/vault/storage`

secrets needed:

- `password.txt`

secrets provided:

- encrypted file with initial root token and unseal keys

tooling:

- script to generate a new autounseal configuration + token `generate_autounseal_token.sh`

**Do not forget to remove `password.txt` after init!!**

## An auto-unseal Vault

We need to provide a `VAULT_SECRETS_DIR`
and a storage for data : `/vault/storage`

Needs secrets => client token for unseal policy, in file `${VAULT_SECRETS_DIR}/autounseal-${VAULT_TRANSIT_KEY_NAME}-encrypted-token.txt`

### with TLS self-signed certificates

```bash
docker run --cap-add=IPC_LOCK -d --env VAULT_X509_SUBJECT="/C=FR/ST=Brittany/L=Brest/O=Article714/OU=Secrets/CN=myvault" --env VAULT_X509_ALTNAMES="DNS:myvault.mydomain.com,DNS:myvault.localhost" -v vault_data:/vault/storage -v vault_logs:/var/log -v vault_config:/container/config --name local_vault article714/vault-container vault-raft
```

## With Volumes to host persistent data

First of all do this:

```bash
docker volume create vault_data
docker volume create vault_config
docker volume create vault_logs
```

To create a container used to deploy a simple Hashicorp Vault with raft mode on a Docker infrastructure

```bash
docker run --cap-add=IPC_LOCK -d --env VAULT_X509_SUBJECT="/C=FR/ST=Brittany/L=Brest/O=Article714/OU=Secrets/CN=myvault" --env VAULT_X509_ALTNAMES="DNS:myvault.mydomain.com,DNS:myvault.localhost" -v vault_data:/vault/storage -v vault_logs:/var/log -v vault_config:/container/config --name local_vault article714/vault-container vault-raft
```

To create a container used to deploy a Hashicorp Vault with raft mode and automatic unlocking on a Docker infrastructure

```bash
docker run --cap-add=IPC_LOCK -d --env VAULT_X509_SUBJECT="/C=FR/ST=Brittany/L=Brest/O=Article714/OU=Secrets/CN=myvault" --env VAULT_X509_ALTNAMES="DNS:myvault.mydomain.com,DNS:myvault.localhost" -v vault_data:/vault/storage -v vault_logs:/var/log -v vault_config:/container/config --name local_vault article714/vault-container vault-transit
```

**Important**: for the containers to be launched, they must have the password.txt file located in /container/config/vault/ during the docker build or use option 1 of the Article714 tool to send the file.

For the initialization of certain vaults and to perform certain actions, it is necessary to use one or more keys created during the initialization of a vault. For security reasons, these are encrypted in a file. To be able to read it, you need the password written in password.txt.
The file is not important, it can be deleted. Just use the Article714 tool to create the password.txt and send it to a vault.

## Tool Article714

The Article714 tool allows you to manage the different vaults:

1. Allows to create and send the password.txt
2. Initialize, unlock and configure a vault
3. Backs up vault configuration and data
4. Initialise2 allows to initialize and unlock a vault after restarting the docker
5. Restore allows loading configuration and data from one vault to another vault
6. Restore2 allows to load a backup in the original vault

## Installing an autounseal vault
